package net.crytec.sb;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.WorldEdit;

import net.crytec.api.InventoryMenuAPI.InvGUI;
import net.crytec.api.InventoryMenuAPI.InvGUI.ROWS;
import net.crytec.api.InventoryMenuAPI.MenuEvent;
import net.crytec.api.devin.commands.Command;
import net.crytec.api.devin.commands.CommandRegistrar;
import net.crytec.api.devin.commands.CommandResult;
import net.crytec.api.devin.commands.Commandable;
import net.crytec.api.itemstack.ItemBuilder;

public class SchematicBrowser extends JavaPlugin implements Listener, Commandable {

	private static final int API_REQUIRED = 207;
	private File SCHEM_FOLDER;
	private CommandRegistrar cr;

	@Override
	public void onEnable() {
		if (!this.checkAPIVersion()) {
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}
		this.cr = new CommandRegistrar(this);
		this.cr.registerCommands(this);

		if (WorldEdit.getInstance().getConfiguration().saveDir.equals("schematics")) {
			SCHEM_FOLDER = WorldEdit.getInstance().getWorkingDirectoryFile(WorldEdit.getInstance().getConfiguration().saveDir);
		} else {
			SCHEM_FOLDER = new File(WorldEdit.getInstance().getConfiguration().saveDir);
		}

		System.out.println(SCHEM_FOLDER.getAbsolutePath());
		
		Bukkit.getPluginManager().registerEvents(this, this);

		new Metrics(this);
	}

	@EventHandler
	public void menu(MenuEvent e) {
		if (!e.getTitle().equals("Schematics")) return;

		if (e.getItem().getType() == Material.ARROW) {
			e.getPlayer().closeInventory();
			this.openSchematicView(e.getPlayer());
			return;
		}

		if (e.getItem().getType() == Material.CHEST) {

			String fname = e.getItemName().replaceAll("Folder: ", "");
			InvGUI gui = new InvGUI(this, e.getPlayer(), "Schematics", ROWS.ROW_6);
			// Folder
			File folder = new File(SCHEM_FOLDER, fname);

			List<ItemStack> tmp = new ArrayList<ItemStack>();

			File[] files = folder.listFiles();
			for (File file : files) {
				if ((!file.isDirectory()) && (file.getAbsolutePath().endsWith(".schematic"))) {

					long size = FileUtils.sizeOf(file);
					String defaulttext = "�aOk";

					if (size > 30000) {
						defaulttext = "�c�lServer could crash!";
					} else if (size > 10000) {
						defaulttext = "�6Server could lag";
					}

					tmp.add(new ItemBuilder(Material.PAPER).name("�6" + fname + "/" + file.getName()).lore("�7Size: " + FileUtils.byteCountToDisplaySize(size)).lore("�7Size Information: "
							+ defaulttext).build());
				}
			}
			gui.addFixedItem(new ItemBuilder(Material.ARROW).name("Back").build(), gui.getInventory().getSize() - 8);
			gui.setPagedInv(tmp, 40, 0);
			gui.openInventory();
			return;
		} else {

			if (e.getItem().getType() == Material.PAPER) {

				String schemname = e.getItemName();
				Bukkit.dispatchCommand(e.getPlayer(), "/schem load " + schemname);
				e.getPlayer().closeInventory();
			}
		}
	}

	private void openSchematicView(Player p) {

		if (!SCHEM_FOLDER.exists()) {
			SCHEM_FOLDER.mkdir();
		}

		InvGUI gui = new InvGUI(this, p, "Schematics", ROWS.ROW_6);
		File[] subfolders = SCHEM_FOLDER.listFiles(File::isDirectory);
		List<ItemStack> tmp = new ArrayList<ItemStack>();
		int x = 0;

		for (File sf : subfolders) {
			gui.addFixedItem(new ItemBuilder(Material.CHEST).name("Folder: " + sf.getName()).build(), x);
			x++;
		}

		File[] files = SCHEM_FOLDER.listFiles();
		for (File file : files) {
			if ((!file.isDirectory()) && (file.getAbsolutePath().endsWith(".schematic"))) {

				long size = FileUtils.sizeOf(file);
				String defaulttext = "�aOk";

				if (size > 30000) {
					defaulttext = "�c�lServer could crash!";
				} else if (size > 10000) {
					defaulttext = "�6Server could lag!";
				}

				tmp.add(new ItemBuilder(Material.PAPER).name("�6" + file.getName()).lore("�7Size: " + FileUtils.byteCountToDisplaySize(size)).lore("�7Info: " + defaulttext).build());
			}
		}

		gui.setPagedInv(tmp, 40, x);
		gui.openInventory();

	}

	@Command(struct = "schematicbrowser", perms = "schematicbrowser.open", aliases = { "sb", "schembrowser" }, desc = "Open the Schematic View Interface")
	public CommandResult schematicBrowserCommand(Player sender) {
		openSchematicView(sender);
		return CommandResult.success();
	}
	
	private boolean checkAPIVersion() {
		if (Bukkit.getPluginManager().getPlugin("CT-Core") != null) {
			String version = Bukkit.getPluginManager().getPlugin("CT-Core").getDescription().getVersion();
			version = version.replaceAll("\\.", "");
			int api_version = Integer.parseInt(version);
			if (api_version < API_REQUIRED) {
				String ver = String.valueOf(API_REQUIRED);
				log("===================================");
				log("�c Unable to load RegionGUI!");
				log("�c You need at least version: �e" + ver.substring(0, 1) + "." + ver.substring(1, 2) + "."
						+ ver.substring(2, 3) + "�c but you have: �6"
						+ Bukkit.getPluginManager().getPlugin("CT-Core").getDescription().getVersion());
				log("�c Please update CT-Core to the latest version if you wish to use this plugin!");
				log("�6 Download the latest version here: https://www.spigotmc.org/resources/ct-core.24842/");
				log("===================================");
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	public void log(String message) {
		log(message, false);
	}
	
	public void log(String message, boolean withPrefix) {
		if (withPrefix) {
		Bukkit.getConsoleSender().sendMessage("[EasyEnchant] " + message);
		return;
		}
		Bukkit.getConsoleSender().sendMessage(message);
	}
	
}
